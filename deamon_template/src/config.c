/* ------------------------------
 * 3CITY ELECTRONICS CONFIDENTIAL
 * ------------------------------
 * Copyright (C) 2018 3City Electronics Sp. z o.o. - All Rights Reserved
 *
 * Unauthorized copying, usage, reproduction or distribution of this file,
 * via any medium is strictly prohibited unless prior written permission
 * is obtained from 3City Electronics Sp. z o.o.
 * ------------------------------
 */

#include <stdio.h>
#include <stdlib.h>
#include <libconfig.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>

#include <sys/syslog.h>
#include <sys/utsname.h>

#include "common.h"
#include "logger.h"
#include "config.h"
#include "version.h"


#define DUMMY_STR_MIN ""
#define DUMMY_STR_MAX ""

#define TEMPLATE_ELEMENT(prefix, prefix_l, suffix, str_name, config_type, value_type, \
    def_val, min_val, max_val)  \
    [prefix##_##suffix] = {.name = str_name, .type = CONFIG_TYPE_##config_type, \
      .parent = &settings_##prefix_l[prefix##_GROUP].setting, \
      .value.value_type = def_val, \
      .value_def.value_type = def_val, \
      .value_min.value_type = min_val, \
      .value_max.value_type = max_val},

#define TE_GROUP(prefix, str_name )  \
  [prefix##_GROUP] = {.name = str_name, .type = CONFIG_TYPE_GROUP, \
    .parent = &config_root.setting},

#define TE_STR(prefix, prefix_l, suffix, str_name, def_val, min_val, max_val)  \
  TEMPLATE_ELEMENT(prefix, prefix_l, suffix, str_name, STRING, _str, def_val, min_val, max_val)
#define TE_INT(prefix, prefix_l, suffix, str_name, def_val, min_val, max_val)  \
  TEMPLATE_ELEMENT(prefix, prefix_l, suffix, str_name, INT, _int, def_val, min_val, max_val)
#define TE_DOUBLE(prefix, prefix_l, suffix, str_name, def_val, min_val, max_val)  \
  TEMPLATE_ELEMENT(prefix, prefix_l, suffix, str_name, FLOAT, _double, def_val, min_val, max_val)

#define LIST_ELEMENT(suffix, suffix_l) \
  [TEMPL_##suffix] = {.templates = settings_##suffix_l, .len = SET_##suffix##_END},

config_t cfg;
char *config_file;

struct config_setting_template_t {
  char *name;
  short type;
  config_setting_t **parent;
  union config_value_u value;
  union config_value_u value_def;
  union config_value_u value_min;
  union config_value_u value_max;
  config_setting_t *setting;
};

struct config_setting_template_list_t {
  struct config_setting_template_t *templates;
  int len;
};

struct config_setting_template_t config_root = {
  .name = "root", .type = CONFIG_TYPE_GROUP, .parent = &config_root.setting,
};

static int validate_min_max(struct config_setting_template_t *template, union config_value_u *value);

#define TE_SET_GLOBAL_SETTINGS(suffix, str_name, type, def_val, min_val, max_val) \
  TE_##type(SET_GLOBAL_SETTINGS, global_settings, suffix, str_name, def_val, min_val, max_val)
struct config_setting_template_t settings_global_settings[] = {
  TE_GROUP(SET_GLOBAL_SETTINGS, "global_settings")
    TE_SET_GLOBAL_SETTINGS(STATUS, "status", INT, APP_STATUS_OK, APP_STATUS_OK, app_status_nr - 1)
    TE_SET_GLOBAL_SETTINGS(LOG, "log", INT, LOGGER_INFO, LOGGER_EMERG, logger_levels_nr - 1)
};

#define TE_SET_USER_ABOUT(suffix, str_name, def_val, min_val, max_val) \
  TE_STR(SET_USER_ABOUT, user_about, suffix, str_name, def_val, min_val, max_val)
struct config_setting_template_t settings_user_about[] = {
  TE_GROUP(SET_USER_ABOUT, "set_user_about") \
    TE_SET_USER_ABOUT(VERSION, "app_ver", "-",  DUMMY_STR_MIN, DUMMY_STR_MAX) \
    TE_SET_USER_ABOUT(HW_VER, "hw_ver", "-",  DUMMY_STR_MIN, DUMMY_STR_MAX) \
    TE_SET_USER_ABOUT(API_VER, "api_ver", "-",  DUMMY_STR_MIN, DUMMY_STR_MAX) \
    TE_SET_USER_ABOUT(KERNEL_VER, "kernel_ver", "-",  DUMMY_STR_MIN, DUMMY_STR_MAX) \
};

struct config_setting_template_list_t templates_list[] = {
  LIST_ELEMENT(GLOBAL_SETTINGS, global_settings)
  LIST_ELEMENT(USER_ABOUT, user_about)
};

static int update_value(config_setting_t *setting, union config_value_u value)
{
  int rv = 0;
  int prev_int = 0;
  const char *prev_str;
  double prev_double = 0;

  switch(setting->type)
  {
    case CONFIG_TYPE_ARRAY:
      ERR("Unsupported config type\n");
      rv = -1;
      break;
    case CONFIG_TYPE_INT:
      rv = config_setting_lookup_int(setting->parent, setting->name, &prev_int);
      if(rv == CONFIG_TRUE)
      {
        if(prev_int != value._int)
        {
          rv = config_setting_set_int(setting, value._int);
          if(rv == CONFIG_TRUE)
          {
            rv = 1;
          }
        }
        else
        {
          rv = 0;
        }
      }
      else
      {
        rv = -1;
      }
      break;
    case CONFIG_TYPE_GROUP:
      break;
    case CONFIG_TYPE_STRING:
      rv = config_setting_lookup_string(setting->parent, setting->name, &prev_str);
      if(rv == CONFIG_TRUE)
      {
        if(strncmp(prev_str, value._str, VAL_MAX_LEN) != 0)
        {
          rv = config_setting_set_string(setting, value._str);
          if(rv == CONFIG_TRUE)
          {
            rv = 1;
          }
        }
        else
        {
          rv = 0;
        }
      }
      else
      {
        rv = -1;
      }
      break;
    case CONFIG_TYPE_BOOL:
      rv = config_setting_lookup_bool(setting->parent,setting->name, &prev_int);
      if(rv == CONFIG_TRUE)
      {
        if(prev_int != value._int)
        {
          rv = config_setting_set_bool(setting, value._int);
          if(rv == CONFIG_TRUE)
          {
            rv = 1;
          }
        }
        else
        {
          rv = 0;
        }
      }
      else
      {
        rv = -1;
      }
      break;
    case CONFIG_TYPE_FLOAT:
      rv = config_setting_lookup_float(setting->parent,setting->name, &prev_double);
      if(rv == CONFIG_TRUE)
      {
        if(prev_double != value._double)
        {
          rv = config_setting_set_float(setting, value._double);
          if(rv == CONFIG_TRUE)
          {
            rv = 1;
          }
        }
        else
        {
          rv = 0;
        }
      }
      else
      {
        rv = -1;
      }
      break;
    default:
      ERR("Unknown config type\n");
      return -1;
  }
  return rv;
}


static int set_default_value(config_setting_t *setting, union config_value_u *value)
{
  int rv = 0;
  switch(setting->type)
  {
    case CONFIG_TYPE_ARRAY:
      ERR("Unsupported config type\n");
      rv = -1;
      break;
    case CONFIG_TYPE_INT:
      rv = config_setting_set_int(setting, value->_int);
      break;
    case CONFIG_TYPE_GROUP:
      rv = CONFIG_TRUE;
      break;
    case CONFIG_TYPE_STRING:
      rv = config_setting_set_string(setting, value->_str);
      break;
    case CONFIG_TYPE_BOOL:
      rv = config_setting_set_bool(setting, value->_int);
      break;
    case CONFIG_TYPE_FLOAT:
      rv = config_setting_set_float(setting, value->_double);
      break;
    default:
      ERR("Unknown config type\n");
      return -1;
  }
  if(rv == CONFIG_FALSE)
  {
    ERR("Could not set config element %s\n", setting->name);
    return -1;
  }
  return 0;
}

int save_configuration_file_path(char *path)
{
  if(! config_write_file(&cfg, path))
  {
    ERR("Error while writing file.\n");
    return -1;
  }

  DBG("New configuration successfully written to: %s\n", path);

  return 0;
}

static int save_configuration_file(void)
{
  return save_configuration_file_path(config_file);
}


int generate_default_conf(void)
{
  struct config_setting_template_list_t *list;
  struct config_setting_template_t *template;

  for (int i = 0; i < TEMPL_END; i++)
  {
    list = &templates_list[i];
    for(int j =0; j < list->len; j++)
    {
      template = &list->templates[j];
      template->setting = config_setting_add(*template->parent, template->name, template->type);
      if(template->setting)
      {
        set_default_value(template->setting, &template->value);
      }
    }
  }

  return save_configuration_file_path(DEFAULT_CONFIG_PATH);
}

static int read_config_element(config_setting_t *setting, union config_value_u *value)
{
  int rv = 0;
  const char *tmp_str;
  switch(setting->type)
  {
    case CONFIG_TYPE_ARRAY:
      ERR("Unsupported config type\n");
      rv = -1;
      break;
    case CONFIG_TYPE_INT:
      if (config_setting_lookup_int(setting->parent,setting->name, &value->_int) < 0)
      {
        rv = -1;
      }
      else
      {
        /*DBG("%s = %d\n", setting->name, value->number);*/
      }
      break;
    case CONFIG_TYPE_GROUP:
      break;
    case CONFIG_TYPE_STRING:
      if (config_setting_lookup_string(setting->parent, setting->name, &tmp_str) < 0)
      {
        rv = -1;
      }
      else
      {
        strncpy(value->_str, tmp_str, VAL_MAX_LEN);
        DBG("%s = %s\n", setting->name, value->_str);
      }
      break;
    case CONFIG_TYPE_BOOL:
      if (config_setting_lookup_bool(setting->parent,setting->name, &value->_int) < 0)
      {
        rv = -1;
      }
      else
      {
        /*DBG("%s = %s\n", setting->name, value->number?"true":"false");*/
      }
      break;
    case CONFIG_TYPE_FLOAT:
      if (config_setting_lookup_float(setting->parent,setting->name, &value->_double) < 0)
      {
        rv = -1;
      }
      else
      {
        DBG("%s = %f\n", setting->name, value->_double);
      }
      break;
    default:
      ERR("Unknown config type\n");
      return -1;
  }
  return rv;
}

int init_config(char *path)
{
  config_init(&cfg);
  config_set_options(&cfg,
      (CONFIG_OPTION_SEMICOLON_SEPARATORS
       | CONFIG_OPTION_COLON_ASSIGNMENT_FOR_GROUPS
       | CONFIG_OPTION_OPEN_BRACE_ON_SEPARATE_LINE));

  config_root.setting = config_root_setting(&cfg);
  config_file = path;
  INF("%s", "Config initialized\n");
  return 0;
}

int deinit_config(void)
{
  config_destroy(&cfg);
  return 0;
}

int read_config_from_file(char *path, config_t *cfg)
{
  struct config_setting_template_t *template;
  struct config_setting_template_list_t *list;
  union config_value_u value;

  /* Read the file. If there is an error, report it and exit. */
  if(! config_read_file(cfg, path))
  {
    ERR("%s:%d - %s\n", config_error_file(cfg),
        config_error_line(cfg), config_error_text(cfg));
    return -1;
  }

  config_root.setting = config_root_setting(cfg);

  for (int i = 0; i < TEMPL_END; i++)
  {
    list = &templates_list[i];
    for(int j = 0; j < list->len; j++)
    {
      template = &list->templates[j];
      if(template == NULL)
      {
        ERR("List element not found\n");
        return -1;
      }
      else
      {
        template->setting = config_setting_get_member(*template->parent, template->name);
        if(template->setting == NULL)
        {
          ERR("Member not found %s \n", template->name);
          return -1;
        }
        else
        {
          read_config_element(template->setting, &value);
          if(validate_min_max(template, &value) < 0)
          {
            update_value(template->setting, template->value_def);
          }
          else
          {
            memcpy(&template->value, &value, sizeof(union config_value_u));
          }
        }
      }
    }
  }
  return 0;
}

int read_config(void)
{
  return read_config_from_file(config_file, &cfg);
}

int get_config_element_max(list_element_t list_element, uint8_t config_element, union config_value_u *value)
{
  struct config_setting_template_list_t *list;
  struct config_setting_template_t *template;
  if(list_element < TEMPL_END)
  {
    list = &templates_list[list_element];
    if(config_element < list->len)
    {
      template = &list->templates[config_element];
      memcpy(value,  &template->value_max, sizeof(union config_value_u));
      if(template->type == CONFIG_TYPE_INT)
      {
        DBG("Element max %s %d\n", template->name, value->_int);
      }
      else if(template->type == CONFIG_TYPE_FLOAT)
      {
        DBG("Element max %s %f\n", template->name, value->_double);
      }
      return 0;
    }
  }
  ERR("Wrong element max id%d %d\n", list_element, config_element);
  return -1;
}

int get_config_element_min(list_element_t list_element, uint8_t config_element, union config_value_u *value)
{
  struct config_setting_template_list_t *list;
  struct config_setting_template_t *template;
  if(list_element < TEMPL_END)
  {
    list = &templates_list[list_element];
    if(config_element < list->len)
    {
      template = &list->templates[config_element];
      memcpy(value,  &template->value_min, sizeof(union config_value_u));
      if(template->type == CONFIG_TYPE_INT)
      {
        DBG("Element min %s %d\n", template->name, value->_int);
      }
      else if(template->type == CONFIG_TYPE_FLOAT)
      {
        DBG("Element min %s %f\n", template->name, value->_double);
      }
      return 0;
    }
  }
  ERR("Wrong element min id %d %d\n", list_element, config_element);
  return -1;
}

int get_config_element_def(list_element_t list_element, uint8_t config_element, union config_value_u *value)
{
  struct config_setting_template_list_t *list;
  struct config_setting_template_t *template;
  if(list_element < TEMPL_END)
  {
    list = &templates_list[list_element];
    if(config_element < list->len)
    {
      template = &list->templates[config_element];
      memcpy(value,  &template->value_def, sizeof(union config_value_u));
      if(template->type == CONFIG_TYPE_INT)
      {
        DBG("Element def %s %d\n", template->name, value->_int);
      }
      else if(template->type == CONFIG_TYPE_FLOAT)
      {
        DBG("Element def %s %f\n", template->name, value->_double);
      }

      return 0;
    }
  }
  ERR("Wrong element def id %d %d\n", list_element, config_element);
  return -1;
}

static int validate_min_max(struct config_setting_template_t *template, union config_value_u *value)
{
  if(template->type == CONFIG_TYPE_INT)
  {
    if(value->_int < template->value_min._int || value->_int > template->value_max._int)
    {
      ERR("Wrong element range %s val: %d min: %d max: %d\n", template->name,
          value->_int, template->value_min._int, template->value_max._int);
      return -1;
    }
  }
  else if(template->type == CONFIG_TYPE_FLOAT)
  {
    if(value->_double < template->value_min._double || value->_double > template->value_max._double)
    {
      ERR("Wrong element range %s val: %f min: %f max: %f\n", template->name,
          value->_double, template->value_min._double, template->value_max._double);
      return -1;
    }
  }
  return 0;
}

int get_config_element(list_element_t list_element, uint8_t config_element, union config_value_u *value)
{
  struct config_setting_template_list_t *list;
  struct config_setting_template_t *template;
  if(list_element < TEMPL_END)
  {
    list = &templates_list[list_element];
    if(config_element < list->len)
    {
      template = &list->templates[config_element];
      read_config_element(template->setting, value);
      if(validate_min_max(template, value) < 0)
      {
        update_value(template->setting, template->value_def);
        return -1;
      }
      return 0;
    }
  }
  ERR("Wrong element cur id %d %d\n", list_element, config_element);
  return -1;
}


int set_config_element(list_element_t list_element, uint8_t config_element, union config_value_u *value)
{
  struct config_setting_template_list_t *list;
  struct config_setting_template_t *template;
  int rv = 0;
  if(list_element < TEMPL_END)
  {
    list = &templates_list[list_element];
    if(config_element < list->len)
    {
      template = &list->templates[config_element];
      if(validate_min_max(template, value) < 0)
      {
        return -1;
      }
      memcpy(&template->value, value, sizeof(union config_value_u));
      rv = update_value(template->setting, template->value);
      if(template->type == CONFIG_TYPE_INT)
      {
        DBG("Element set %s %d\n", template->name, value->_int);
      }
      else if(template->type == CONFIG_TYPE_FLOAT)
      {
        DBG("Element set %s %f\n", template->name, value->_double);
      }
      if(rv == 1)
      {
        rv = save_configuration_file();
      }
      else if(rv == 0)
      {
        /*DBG("Not changed element %d %d\n", list_element, config_element);*/
      }
      else
      {
        ERR("Could not read element %d %d\n", list_element, config_element);
      }
      return rv;
    }
  }
  ERR("Wrong element set id %d %d\n", list_element, config_element);
  return -1;
}

int handle_config_element_change(uint8_t list_element, uint8_t config_element)
{
  return 0;
}

int set_config_about(config_user_about_t option, char *value)
{
  union config_value_u temp;
  strncpy(temp._str, value, VAL_MAX_LEN);
  set_config_element(TEMPL_USER_ABOUT, option, &temp);
  return 0;
}

int import_config_from_file(char *path)
{
  if(read_config_from_file(path, &cfg) < 0)
  {
    return -1;
  }
  return save_configuration_file();
}


void config_about_runtime_constants()
{
  struct utsname uname_buf;
  char buf[VAL_MAX_LEN];
  uname(&uname_buf);
  set_config_about(SET_USER_ABOUT_VERSION, APP_VER" "COMPILE_TYPE" "COMPILE_TIME);
  snprintf(buf, VAL_MAX_LEN, "%s %s", HW_VERSION, uname_buf.machine);
  set_config_about(SET_USER_ABOUT_HW_VER, buf);
  set_config_about(SET_USER_ABOUT_API_VER, "1.0");
  set_config_about(SET_USER_ABOUT_KERNEL_VER, uname_buf.release);
}
