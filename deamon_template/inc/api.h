/* ------------------------------
 * * 3CITY ELECTRONICS CONFIDENTIAL
 * * ------------------------------
 * * Copyright (C) 2018 3City Electronics Sp. z o.o. - All Rights Reserved
 * *
 * * Unauthorized copying, usage, reproduction or distribution of this file,
 * * via any medium is strictly prohibited unless prior written permission
 * * is obtained from 3City Electronics Sp. z o.o.
 * * ------------------------------
 * */

#ifndef API_H_TJANSERT
#define API_H_TJANSERT

#define VAL_MAX_LEN 255

#include <stdint.h>

union config_value_u {
  char _str[VAL_MAX_LEN];
  int _int;
  double _double;
};

enum config_global_settings {
  SET_GLOBAL_SETTINGS_GROUP,
  SET_GLOBAL_SETTINGS_STATUS,
  SET_GLOBAL_SETTINGS_LOG,
  SET_GLOBAL_SETTINGS_END
};
typedef enum config_global_settings config_global_settings_t;

enum config_user_about {
  SET_USER_ABOUT_GROUP,
  SET_USER_ABOUT_VERSION,
  SET_USER_ABOUT_HW_VER,
  SET_USER_ABOUT_API_VER,
  SET_USER_ABOUT_KERNEL_VER,
  SET_USER_ABOUT_END
};
typedef enum config_user_about config_user_about_t;

typedef enum {
  TEMPL_GLOBAL_SETTINGS,
  TEMPL_USER_ABOUT,
  TEMPL_END
} list_element_t;
#endif

