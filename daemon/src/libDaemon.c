#include <stdio.h>
#include <signal.h>
#include <syslog.h>
#include <errno.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <common.h>
#include <libDaemon.h>

#define MAX_PID_LENGTH 10


void signal_handler(int sig);

void signal_handler(int sig)
{
  switch(sig)
  {
    case SIGHUP:
      syslog(LOG_WARNING, "Received SIGHUP signal.");
      break;
    case SIGINT:
    case SIGTERM:
      syslog(LOG_INFO, "Daemon exiting");
      daemonShutdown();
      exit(EXIT_SUCCESS);
      break;
    default:
      syslog(LOG_WARNING, "Unhandled signal %s", strsignal(sig));
      break;
  }
}

int createPidFile(char *pidfile)
{
  char str[MAX_PID_LENGTH];
  FILE *pidFilePointer;
  sprintf(str,"%d\n",getpid());

  int fd;
  if ((fd = open(pidfile, O_RDWR | O_CREAT, 0600)) >= 0)
    pidFilePointer = fdopen(fd, "w+");
  if(pidFilePointer <= 0)
  {
    syslog(LOG_INFO, "Could not open PID lock file %s, exiting", pidfile);
    exit(EXIT_FAILURE);
  }

  if(fwrite(str, sizeof(char), strlen(str), pidFilePointer) < 0)
  {
    syslog(LOG_INFO, "Could not write PID lock file %s, exiting", pidfile);
    exit(EXIT_FAILURE);
  }

  fclose(pidFilePointer);
  return 0;
}

int checkIfProcessExists(char *pidfile)
{
  char str[MAX_PID_LENGTH];
  FILE *pidFilePointer;
  pidFilePointer = fopen(pidfile, "r");

  if(pidFilePointer < 0)
  {
    syslog(LOG_INFO, "Could not open PID lock file %s, exiting", pidfile);
    exit(EXIT_FAILURE);
  }

  if(fgets(str, MAX_PID_LENGTH, pidFilePointer) < 0)
  {
    syslog(LOG_INFO, "Could not open PID lock file %s, exiting", pidfile);
    exit(EXIT_FAILURE);
  }
  fclose(pidFilePointer);
  pid_t pidFromFile = (pid_t)atoi(str);
  syslog(LOG_INFO, "Pid from file %d \n", pidFromFile);
  int signalResponse = 0;
  if((signalResponse = kill(pidFromFile, 0)) == 0)
  {
    syslog(LOG_INFO, "Process exists %s \n", pidfile);
    return 1;
  }
  else if (signalResponse == ESRCH)
  {
    syslog(LOG_INFO, "Process not exists %s \n", pidfile);
    return 0;
  }
  else
  {
    syslog(LOG_INFO, "Process not responding %s \n", pidfile);
    return -1;
  }
}

int pidFileProcess(char *pidfile)
{
  int processExists;
  int retval;
  if( access( pidfile, F_OK ) != -1 )
  {
    if((processExists = checkIfProcessExists(pidfile) == 1))
    {
      syslog(LOG_INFO, "Process already exists and is responding %s\n", pidfile);
      exit(EXIT_FAILURE);
    }
    else if (processExists == 0)
    {
      retval = createPidFile(pidfile);
    }
    else
    {
      syslog(LOG_ERR, "Process already exists and is not responding %s\n", pidfile);
      exit(EXIT_FAILURE);
    }
  }
  else
  {
    retval = createPidFile(pidfile);
  }
  return retval;
}

void daemonShutdown()
{
}

void daemonize(char *rundir, char *daemonName)
{
  int pid, sid, i;
  char pidfile[128];
  struct sigaction newSigAction;
  sigset_t newSigSet;

  /* Check if parent process id is set */
  if (getppid() == 1)
  {
    /* PPID exists, therefore we are already a daemon */
    return;
  }

  /* Set signal mask - signals we want to block */
  sigemptyset(&newSigSet);
  sigaddset(&newSigSet, SIGCHLD);  /* ignore child - i.e. we don't need to wait for it */
  sigaddset(&newSigSet, SIGTSTP);  /* ignore Tty stop signals */
  sigaddset(&newSigSet, SIGTTOU);  /* ignore Tty background writes */
  sigaddset(&newSigSet, SIGTTIN);  /* ignore Tty background reads */
  sigprocmask(SIG_BLOCK, &newSigSet, NULL);   /* Block the above specified signals */

  /* Set up a signal handler */
  newSigAction.sa_handler = signal_handler;
  sigemptyset(&newSigAction.sa_mask);
  newSigAction.sa_flags = 0;

  /* Signals to handle */
  sigaction(SIGHUP, &newSigAction, NULL);     /* catch hangup signal */
  sigaction(SIGTERM, &newSigAction, NULL);    /* catch term signal */
  sigaction(SIGINT, &newSigAction, NULL);     /* catch interrupt signal */

  /* Fork*/
  pid = fork();

  if (pid < 0)
  {
    /* Could not fork */
    exit(EXIT_FAILURE);
  }

  if (pid > 0)
  {
    /* Child created ok, so exit parent process */
    printf("Child process created: %d\n", pid);
    exit(EXIT_SUCCESS);
  }

  /* Child continues */

  umask(027); /* Set file permissions 750 */

  /* Get a new process group */
  sid = setsid();

  if (sid < 0)
  {
    exit(EXIT_FAILURE);
  }

  /* close all descriptors */
  for (i = getdtablesize(); i >= 0; --i)
  {
    close(i);
  }

  /* Route I/O connections */

  /* Open STDIN */
  i = open("/dev/null", O_RDWR);

  /* STDOUT */
  dup(i);

  /* STDERR */
  dup(i);

  chdir(rundir); /* change running directory */

  snprintf(pidfile, 128,  "/tmp/%s.pid", daemonName);

  if(pidFileProcess(pidfile) < 0)
  {
    syslog(LOG_ERR, "Error processing pid file. Exiting %s \n", pidfile);
    exit(EXIT_FAILURE);
  }

}


