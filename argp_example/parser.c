#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>

#define COMMAND_ARGS_SIZE (sizeof(command_args)/sizeof(command_args_t))

static int parse_set_option(char *subopts, uint16_t reqArgs, const char *usage);
static int parse_get_firmware(char *subopts, uint16_t reqArgs, const char *usage);
typedef enum {
  KEY_GET_FIRMWARE=1,
  KEY_SET_OPTIONS,
} command_arg_key_t;

typedef struct _command_args_s {
  const command_arg_key_t key;
  const uint16_t reqArgs;
  const char *name;
  int (*function)(char*, uint16_t, const char*);
  const char *usage;
} command_args_t;

const command_args_t command_args[] =
{
  {KEY_GET_FIRMWARE, 0, "get-firmware", parse_get_firmware, "get-firmware"},
  {KEY_SET_OPTIONS, 3, "set-options", parse_set_option, "set-options ro,rw,name=<name>"},
  {0, 0, 0, 0}
};

static int parse_get_firmware(char *subopts, uint16_t reqArgs, const char *usage)
{
  return 0;
}

static int parse_set_option(char *subopts, uint16_t reqArgs, const char *usage)
{
  char *value;
  int errfnd = 0;
  int opt = 0;
  int subarg_cnt = 0;
  enum {
    RO_OPT = 0,
    RW_OPT,
    NAME_OPT
  };
  char *const token[] = {
    [RO_OPT]   = "ro",
    [RW_OPT]   = "rw",
    [NAME_OPT] = "name",
    NULL
  };
  while (*subopts != '\0' && !errfnd)
  {
    subarg_cnt++;
    opt = getsubopt(&subopts, token, &value);
    switch (opt)
    {
      case RO_OPT:
        printf("ro opt\n");
        break;

      case RW_OPT:
        printf("rw opt\n");
        break;

      case NAME_OPT:
        if (value == NULL) {
          fprintf(stderr, "Missing value for "
              "suboption '%s'\n", token[NAME_OPT]);
          errfnd = 1;
          break;
        }
        printf("VAL: %s\n", value);
        break;

      default:
        fprintf(stderr, "No match found "
            "for token: /%s/\n", value);
        errfnd = 1;
        break;
    }
  }
  if(errfnd || subarg_cnt != reqArgs)
  {
    printf("USAGE: \n %s\n", usage);
  }
  return errfnd;
}

int main (int argc, char **argv)
{
  int c,i;
  int errfnd = 0;
  static struct option *long_options;
  long_options = (struct option *)malloc(sizeof(struct option)*COMMAND_ARGS_SIZE);
  for (i = 0; i < COMMAND_ARGS_SIZE; i++)
  {
    long_options[i].name = command_args[i].name;
    long_options[i].has_arg = command_args[i].reqArgs==0?no_argument:required_argument;
    long_options[i].flag = 0;
    long_options[i].val = command_args[i].key;
  }

  while (1)
  {
    int option_index = 0;

    c = getopt_long (argc, argv, "",
        long_options, &option_index);

    if (c == -1)
      break;

    for (i = 0; i < COMMAND_ARGS_SIZE; i++)
    {
      const command_args_t *command_arg = &command_args[i];
      if( c == command_arg->key)
      {
        printf("Command: %s\n", command_arg->name);
        errfnd = command_arg->function(optarg, command_arg->reqArgs, command_arg->usage);
        break;
      }
    }
    if(errfnd != 0)
    {
      printf("Error encountered\n");
      break;
    }
    if( i == COMMAND_ARGS_SIZE )
    {
      printf("Unknown command\n");
      break;
    }
  }
  free(long_options);
  exit (0);
}
