#!/bin/bash

GREEN='\033[1;32m'
GREEN_B='\033[0;32m'
BLUE='\033[1;34m'
RED='\033[1;31m'
RED_B='\033[0;31m'
NC='\033[0m' # No Color
LOG="1>> out.log 2>> err.log"
INFO="Build"
pathpat="(/[^/]*)+:[0-9]+" 
ccred=$(echo -e "\033[0;31m") 
ccblue=$(echo -e "\033[1;34m") 
ccyellow=$(echo -e "\033[0;33m") 
ccend=$(echo -e "\033[0m") 

function printInfoColour
{
  echo -n -e "${GREEN}"; cat out.log | sed -E -e "/[Ee]rror[: ]/ s%$pathpat%$ccred&$ccend%g" -e "/[Ww]arning[: ]/ s%$pathpat%$ccyellow&$ccend%g" -e "/[Nn]ote[: ]/ s%$pathpat%$ccblue&$ccend%g" ; echo -e -n "${NC}"
  cat err.log | sed -E -e "/[Ee]rror[: ]/ s%$pathpat%$ccred&$ccend%g" -e "/[Uu]ndefined reference to[: ]/ s%$pathpat%$ccred&$ccend%g" -e "/[Ww]arning[: ]/ s%$pathpat%$ccyellow&$ccend%g" -e "/[Nn]ote[: ]/ s%$pathpat%$ccblue&$ccend%g"  
} 

function printStatus
{
    # We need to waterfall make EXIT CODE to external scripts
case $make_exit_status in
    0)
       echo -e -n "${GREEN_B}"; echo 'Successful' >&2 ; echo -e -n "${NC}"
        exit 0
        ;;
    *)
        echo -e -n "${RED_B}";echo "Error $make_exit_status, exiting." >&2 ; echo -n -e "${NC}"
        exit "$make_exit_status"
esac
}

while [ "$1" != "" ]; do
    case $1 in
        -c )              CLEAN=clean
                          FLAGC=-c
                          INFO="Clean"
                                ;;
        -t )              eval ctags -R *
                          echo "Tags created"
                                ;;
        -r )              REB=true
                                ;;
        -d )              DEB="DEBUG=y"
                                ;;
        * )             exit 1
    esac
    shift
done

if [ $REB ]; then
  eval ./build.sh -c $OPT 
fi

echo -e "${BLUE}${INFO} for Linux${NC}" 
eval make $CLEAN $DEB $LOG
make_exit_status=$?
printInfoColour
rm -f out.log err.log
printStatus
