"Project specific .vimrc file for ultimate, generic programmers.
"Add "set exrc" line to your global .vimrc file
"Author Mateusz Orzoł 2017
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"       Use whitelist or only exclude files matching exclude patterns?       "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:useWhitelist = 1
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"       List of filetypes and single files used for whitelist pattern        "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let projectFiletypes = ['c', 'h', 'py', 'js', 'sh']
let projectFiles = ['Makefile', 'config.conf', '.vimrc']
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"     List of filetypes, files and directories used in blacklist pattern     "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let excludedFiletypes = ['o', 'disasm', 'hex', 'readelf', 'bin', 'exe', 'so', 'dll', 'pyc']
let excludedFiles = ['tags']
let excludedDirectories = ['obj', '.hidden', '.svn', '.git', 'public_bak']
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                     ctrlp working path mode [c/a/r/w]                      "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:ctrlp_working_path_mode = 'a'
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                            Intendation options                             "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set tabstop=2 shiftwidth=2 expandtab
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                         Custom syntax highlighting                         "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
autocmd BufNewFile,BufRead *.conf set syntax=sh
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                            Custom make commands                            "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set makeprg=./build.sh
nnoremap <F7> :make -r -d <CR>
nnoremap <F6> :make -c <CR>
nnoremap <F9> :make -r -d -s <CR>
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                              Implemetnations                               "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:vimgrepFiletypes = join(map(copy(projectFiletypes), 'substitute(v:val,".*","./**/*.\\0","")'))
let g:vimgrepFiles = join(map(copy(projectFiles), 'substitute(v:val,".*","./**/*\\0","")'))
let g:vimgrepWhitelist = g:vimgrepFiletypes.' '.g:vimgrepFiles

let g:ctrlpFiletypes = join(map(copy(projectFiletypes), 'substitute(v:val,".*","\\\\\(.*\\\\\.\\0\\\\\)","")'),"\\|")
let g:ctrlpFiles = join(map(copy(projectFiles), 'substitute(v:val,".*","\\\\\(.*\\0\\\\\)","")'),"\\|")
let g:ctrlpWhitelist = g:ctrlpFiletypes.'\|'.g:ctrlpFiles

let g:ctrlpExcFiles = join(map(copy(excludedFiles), 'substitute(v:val,".*","\\\\\(.*\\0\\\\\)","")'),"\\|")
let g:ctrlpExcFiletypes = join(map(copy(excludedFiletypes), 'substitute(v:val,".*","\\\\\(.*\\\\\.\\0\\\\\)","")'),"\\|")
let g:ctrlpExcDirectories = join(map(copy(excludedDirectories), 'substitute(v:val,".*","\\\\\(.*\\0\\\\\)","")'),"\\|")
let g:ctrlpBlacklist = g:ctrlpExcFiletypes.'\|'.g:ctrlpExcFiles.'\|'.g:ctrlpExcDirectories

fu! FindHighlightedInFiles(extensions)
  exe "silent vimgrep! /" . expand("<cword>").'/gj' . a:extensions
  exe "cw"
endfu
com! FindHighlightedInFiles call FindHighlightedInFiles(g:vimgrepWhitelist)
nnoremap <F5> :FindHighlightedInFiles<CR>

fu! s:FindInFiles(extensions)
  echohl MatchParen
  let userInput = input(">>>Search in project:")
  echohl None
  if userInput == ''
    echohl Error
    echon 'Empty input'
    echohl None
    return
  endif
  exe "silent vimgrep! /" . userInput .'/gj' . a:extensions
  exe "cw"
endfu
com! FindInFiles call s:FindInFiles(g:vimgrepWhitelist)
nnoremap <leader>/ :FindInFiles<CR>

if g:useWhitelist == 1
  let g:ctrlp_user_command = 'find %s -regex "'.g:ctrlpBlacklist.'" -prune -o -type f -regex "'.g:ctrlpWhitelist.'" -print'
else
  let g:ctrlp_user_command = 'find %s -regex "'.g:ctrlpBlacklist.'" -prune -o -type f -regex ".*" -print'
endif
