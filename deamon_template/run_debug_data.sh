#/* ------------------------------ 
#* 3CITY ELECTRONICS CONFIDENTIAL 
#* ------------------------------ 
#* Copyright (C) 2018 3City Electronics Sp. z o.o. - All Rights Reserved 
#* 
#* Unauthorized copying, usage, reproduction or distribution of this file, 
#* via any medium is strictly prohibited unless prior written permission 
#* is obtained from 3City Electronics Sp. z o.o. 
#* ------------------------------ 
#./debug.sh -a 238 -r /home/root/data-broker/armv7-db-test-app -f armv7-db-test-app
IP_ADDR=$(ssh -G bringup | awk '/^hostname / { print $2}' | tail -c 4)
./debug.sh -a $IP_ADDR -r /home/root/data-broker/armv7-data-broker-app -f armv7-data-broker-app 
