#ifndef LIBDAEMON_H_M89VAJLR
#define LIBDAEMON_H_M89VAJLR

void daemonShutdown();
void daemonize(char *rundir, char *daemonName);


#endif /* end of include guard: LIBDAEMON_H_M89VAJLR */
