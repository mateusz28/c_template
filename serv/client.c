#include <stdio.h> //obsluga wejsc oraz wyjsc
#include <stdlib.h> //funkcje generalnego uzytku, dynamiczna alokacja
//pamieci, arytmetyka liczb calkowitych, komunikacja z otoczeniem
#include <string.h>//przetwarzanie ciagow znakow oraz tablic
#include <unistd.h>//uslugi systemu operacyjnego oraz makra
#include <sys/types.h>//zawiera definicje typow wykorzystywanych przy 
//wywolaniach systmowych
#include <sys/socket.h>//struktury wymagane przy tworzeniu gniazd
#include <netinet/in.h>//zmienne oraz struktury do adresowania 
#include <netdb.h>//zdefiniowana jest struktura hostent

#define BUFFER_SIZE 256
#define QUEUED_CONNECTIONS_LIMIT 5
#define PORT_NUMBER 5000
#define HOST_NAME "localhost"

int main(int argc, char *argv[])
{
	int sockfd; //deskryptor plikow, przechowuje wartosci zwrocone 
				//przez wywolania systemowe call oraz accept 
	
	int portno; //przechowuje numer portu, na którym serwer akceptuje połączenia
	
	int length; //przechowuje wartosc zwrocona przez funkcje read i write, tj. 
		//liczbe znakow odczytanych lub zapisanych
	
	//sockaddr_in - struktura zawierająca adres, jest ona zdefiniowana w pliku nagłówkowym netinet/in.h.
	struct sockaddr_in serv_addr; //przechowuje adres serwera
	
	struct hostent *server;//zmienna server jest wskaznikiem na strukture hostnetzdefiniowana w pliku naglowkowym netdb.h_addr
	//struktura ta definiuje urzadzenie w internecie
	
	char buffer[BUFFER_SIZE]; //klient wpisuje lub sczytuje dane z gniazda do tego bufora
	
	//portno = atoi("5000"); //numer portu, na ktorym serwer będzie nasluchiwal połączenia jest przekazany, jako
	//argument, a funkcja atoi() dokonuje konwersji z ciagu znaków na liczbę calkowita, zamiast tego mozna zapisac
	portno = PORT_NUMBER;

	sockfd = socket(AF_INET, SOCK_STREAM,0); //tworzenie nowego gniazda,
	// pierwszy argument AF_UNIX - do komunikacji na tej samej maszynie lub AF_INET - pomiędzy dwoma maszynami ipv4, AF_INET6 - ip6
	// drugi argument SOCK_STREAM - rodzaj gniazda, w tym przypadku gniazdo strumieniowe
	// trzeci argument - protokol, kiedy wartosc wynosi 0 to system sam wybierze najbardziej wlasciwy protokol
	
	server = gethostbyname(HOST_NAME);//Funkcja gethostbyname bobiera nazwe hosta 

	bzero((char *)&serv_addr, sizeof(serv_addr)); //Funkcja bzero zeruje wszystkie wartości bufora,
	//pierwszym argumentem jest wskaźnik na bufor, a drugim rozmiar bufora
	
	//zmienna serv_addr jest zmienna typu struct sockaddr_in, struktura ta posiada cztery pola
	serv_addr.sin_family = AF_INET; //pierwszym jest short sin_family, zawiera kod rodziny adresowej
	
	bcopy((char*)server->h_addr,(char*)&serv_addr.sin_addr.s_addr,server->h_length); //przypisanie zmiennych do struktury
	
	serv_addr.sin_port = htons(portno);//zawiera numer portu, na ktorym bedzie odbywala sie komunikacja,
	//numer portu w network byte order zamiast standardowego host byte, do tej zamiany sluzy htons(nr portu)
	
	connect(sockfd,(struct sockaddr *)&serv_addr, sizeof(serv_addr)); //wykorzystywane do polaczenia z serverem,
	//pierwszy argument - deskryptor pliku gniazda,
	//drugi - adres hosta
	//trzeci rozmiar adresu
	
	printf("Please enter the message: ");//wyswietla etykiete
	bzero(buffer,BUFFER_SIZE); //zerowanie bufora
	fgets(buffer,BUFFER_SIZE-1,stdin); //sluzy do odczytania wiadomosci z wejscia
	length=write(sockfd,buffer,strlen(buffer)); //zapis wiadomosci do gniazda
	
	bzero(buffer,BUFFER_SIZE); //zerowanie bufora przed odczytem wiadomosci z gniazda
	length=read(sockfd,buffer,BUFFER_SIZE-1); //read sluzy do odczytu wiadomosci z gniazda 

	printf("%s\n",buffer);//wypisywanie odczytanej wiadomosci
	close(sockfd); //zamkniecie deskryptora - gniazda, zakonczenie programu
return 0;
}
