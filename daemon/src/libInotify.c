#include <sys/inotify.h>
#include <limits.h>

#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <syslog.h>

#include <common.h>
#include <libInotify.h>
static void displayInotifyEvent(struct inotify_event *i)
{
  if (i->cookie > 0)
    syslog(LOG_INFO, "cookie =%4d; ", i->cookie);

  if (i->mask & IN_ACCESS)        syslog(LOG_INFO, "IN_ACCESS ");
  if (i->mask & IN_ATTRIB)        syslog(LOG_INFO, "IN_ATTRIB ");
  if (i->mask & IN_CLOSE_NOWRITE) syslog(LOG_INFO, "IN_CLOSE_NOWRITE ");
  if (i->mask & IN_CLOSE_WRITE)   syslog(LOG_INFO, "IN_CLOSE_WRITE ");
  if (i->mask & IN_CREATE)        syslog(LOG_INFO, "IN_CREATE ");
  if (i->mask & IN_DELETE)        syslog(LOG_INFO, "IN_DELETE ");
  if (i->mask & IN_DELETE_SELF)   syslog(LOG_INFO, "IN_DELETE_SELF ");
  if (i->mask & IN_IGNORED)       syslog(LOG_INFO, "IN_IGNORED ");
  if (i->mask & IN_ISDIR)         syslog(LOG_INFO, "IN_ISDIR ");
  if (i->mask & IN_MODIFY)        syslog(LOG_INFO, "IN_MODIFY ");
  if (i->mask & IN_MOVE_SELF)     syslog(LOG_INFO, "IN_MOVE_SELF ");
  if (i->mask & IN_MOVED_FROM)    syslog(LOG_INFO, "IN_MOVED_FROM ");
  if (i->mask & IN_MOVED_TO)      syslog(LOG_INFO, "IN_MOVED_TO ");
  if (i->mask & IN_OPEN)          syslog(LOG_INFO, "IN_OPEN ");
  if (i->mask & IN_Q_OVERFLOW)    syslog(LOG_INFO, "IN_Q_OVERFLOW ");
  if (i->mask & IN_UNMOUNT)       syslog(LOG_INFO, "IN_UNMOUNT ");

  if (i->len > 0)
    syslog(LOG_INFO, "name = %s\n", i->name);
}

#define BUF_LEN (10 * (sizeof(struct inotify_event) + NAME_MAX + 1))

int addFileNotification(char *filepath)
{
  int inotifyFd, wd;
  char buf[BUF_LEN] __attribute__ ((aligned(8)));
  ssize_t numRead;
  char *p;
  struct inotify_event *event;


  inotifyFd = inotify_init();                 /* Create inotify instance */
  if (inotifyFd == -1)
    syslog(LOG_INFO, "inotify_init");

  wd = inotify_add_watch(inotifyFd, filepath, IN_ALL_EVENTS);
  syslog(LOG_INFO, "Added watch to file \n");
  if (wd == -1)
  {
    syslog(LOG_ERR, "inotify_add_watch");
    return -1;
  }
  syslog(LOG_INFO, "Watching %s using wd %d\n", filepath , wd);

  for (;;) {                                  /* Read events forever */
    numRead = read(inotifyFd, buf, BUF_LEN);
    if (numRead == 0)
    {
      syslog(LOG_ERR,"read() from inotify fd returned 0!");
      return -1;
    }

    if (numRead == -1)
    {
      syslog(LOG_ERR, "read");
      return -1;
    }

    syslog(LOG_INFO, "Read %ld bytes from inotify fd\n", (long) numRead);

    /* Process all of the events in buffer returned by read() */

    for (p = buf; p < buf + numRead; ) {
      event = (struct inotify_event *) p;
      displayInotifyEvent(event);

      p += sizeof(struct inotify_event) + event->len;
    }
    sleep(1);
  }

  return 0;
}
