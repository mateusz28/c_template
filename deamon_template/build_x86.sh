#/* ------------------------------ 
#* 3CITY ELECTRONICS CONFIDENTIAL 
#* ------------------------------ 
#* Copyright (C) 2018 3City Electronics Sp. z o.o. - All Rights Reserved 
#* 
#* Unauthorized copying, usage, reproduction or distribution of this file, 
#* via any medium is strictly prohibited unless prior written permission 
#* is obtained from 3City Electronics Sp. z o.o. 
#* ------------------------------ 
#*/
#!/bin/bash
DEBUG_OPT=y
. ./environment-setup-armv7at2hf-neon-fslc-linux-gnueabi
make clean ARCH=x86 DEBUG=${DEBUG_OPT}
make ARCH=x86 DEBUG=${DEBUG_OPT} ESCCHAR=y
#scp armv7-db-test-app root@192.168.11.215:data-broker/
#ssh root@192.168.11.215 -t "pkill armv7"
#scp armv7-data-broker-app root@192.168.11.215:data-broker/
#scp armv7-db-test-app root@192.168.11.215:data-broker/
