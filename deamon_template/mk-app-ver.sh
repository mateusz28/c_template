#/* ------------------------------ 
#* 3CITY ELECTRONICS CONFIDENTIAL 
#* ------------------------------ 
#* Copyright (C) 2018 3City Electronics Sp. z o.o. - All Rights Reserved 
#* 
#* Unauthorized copying, usage, reproduction or distribution of this file, 
#* via any medium is strictly prohibited unless prior written permission 
#* is obtained from 3City Electronics Sp. z o.o. 
#* ------------------------------ 
#!/bin/bash
#auto versioning script
#gets values from given (normally: version.h) file
# increments BUILD part of the version number
# sets compilation time and machine name
# rewrites all other original fields
# updates given file with new values
cp -f $1 $1.bak
rm -f $1
linia=`awk 'BEGIN{ FS=" " } { if( $(NF-1) == "APP_VER" ) print $NF; }' $1.bak`
linia=`echo $linia | awk 'BEGIN{ FS="\"" } { print $(NF-1) }'`
build=` echo $linia | awk 'BEGIN{ FS="." } { print $NF }'`
minor=` echo $linia | awk 'BEGIN{ FS="." } { print $(NF-1) }'`
major=` echo $linia | awk 'BEGIN{ FS="." } { print $(NF-2) }'`
build=$(($build+1))
echo "#define COMPILE_TIME \"`date +%Y"-"%m"-"%d" "%H":"%M":"%S`\"" > $1
echo "#define COMPILE_MACHINE_NAME \"`hostname`\"" >> $1
echo "#define APP_VER \"$major.$minor.$build\"" >> $1
echo "#define COMPILE_TYPE \"$2\"" >> $1
echo "#define HW_VERSION \"SOGS3\"" >> $1
echo "#define API_VERSION_STR \"1.0\"" >> $1

rm -f $1.bak
