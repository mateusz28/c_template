#/* ------------------------------ 
#* 3CITY ELECTRONICS CONFIDENTIAL 
#* ------------------------------ 
#* Copyright (C) 2018 3City Electronics Sp. z o.o. - All Rights Reserved 
#* 
#* Unauthorized copying, usage, reproduction or distribution of this file, 
#* via any medium is strictly prohibited unless prior written permission 
#* is obtained from 3City Electronics Sp. z o.o. 
#* ------------------------------ 
#*/
#!/bin/bash
DEBUG_OPT=y
make clean ARCH=x86 DEBUG=${DEBUG_OPT}
make ARCH=x86 DEBUG=${DEBUG_OPT} ESCCHAR=y
#ssh sogs -t "pkill legic-app"
#scp legic-app sogs:data-broker/
