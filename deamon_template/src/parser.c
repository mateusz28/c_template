/* ------------------------------ 
   * 3CITY ELECTRONICS CONFIDENTIAL 
   * ------------------------------ 
   * Copyright (C) 2018 3City Electronics Sp. z o.o. - All Rights Reserved 
   * 
   * Unauthorized copying, usage, reproduction or distribution of this file, 
   * via any medium is strictly prohibited unless prior written permission 
   * is obtained from 3City Electronics Sp. z o.o. 
   * ------------------------------ 
   */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <syslog.h>

#include "common.h"
#include "version.h"
#include "parser.h"
#include "logger.h"


#define COMMAND_ARGS_SIZE (sizeof(command_args)/sizeof(command_args_t))
#define DELIMETER "--------------------------------------------------------------------------"

static int numeric_subopt_parse(char *value, char *name, int *ret_val, int limit_down, int limit_top);
static int __attribute__((unused)) string_subopt_parse(char *value, char *name, char *ret_val, int max_length);
static int parse_help(char *subopts, uint16_t req_args, const char *usage, struct broker_args *config);
static int parse_version(char *subopts, uint16_t req_args, const char *usage, struct broker_args *config);
static int parse_daemon(char *subopts, uint16_t req_args, const char *usage, struct broker_args *config);
static int generate_conf(char *subopts, uint16_t req_args, const char *usage, struct broker_args *config);
static int set_log_level(char *subopts, uint16_t req_args, const char *usage, struct broker_args *config);



typedef enum {
  KEY_HELP =1,
  KEY_VERSION,
  KEY_DAEMON,
  KEY_GENERATE,
  KEY_LOG_LEVEL
} command_arg_key_t;

typedef struct _command_args_s {
  const command_arg_key_t key;
  const uint16_t req_args;
  const char *name;
  int (*function)(char*, uint16_t, const char*, struct broker_args*);
  const char *usage;
} command_args_t;

const command_args_t command_args[] =
{
  {KEY_HELP, 0, "help", parse_help, "--help"},
  {KEY_VERSION, 0, "version", parse_version, "--version"},
  {KEY_DAEMON, 0, "daemon", parse_daemon, "--daemon"},
  {KEY_GENERATE, 0, "generate", generate_conf, "--generate"},
  {KEY_LOG_LEVEL, 1, "log", set_log_level, "--log level=<0-7> (0- emerg; 7-debug; 8-file)"},
};

static int string_subopt_parse(char *value, char *name, char *ret_val, int max_length)
{
  int errfnd = 0;
  if (value == NULL) {
    printf( "Missing value for "
        "suboption '%s'\n", name);
    errfnd = 1;
    return errfnd;
  }
  strncpy(ret_val, value, max_length);
  printf("%s: %s\n", name, ret_val);
  return 0;
}

static int __attribute__((unused)) numeric_subopt_parse(char *value, char *name, int *ret_val, int limit_down, int limit_top)
{
  int errfnd = 0;
  if (value == NULL) {
    printf( "Missing value for "
        "suboption '%s'\n", name);
    errfnd = 1;
    return errfnd;
  }
  *ret_val = strtol(value, NULL, 10);
  if(*ret_val <= limit_top && *ret_val >= limit_down)
  {
    printf("%s: %d\n", name, *ret_val);
  }
  else
  {
    printf("%s wrong value %d\n", name, *ret_val);
    errfnd=1;
  }
  return errfnd;
}

void print_help(char *app_name)
{
  int i;
  printf("%s usage:\n", app_name);
  printf("%s\n",DELIMETER);
  for (i = 0; i < COMMAND_ARGS_SIZE; i++)
  {
    const command_args_t *command_arg = &command_args[i];
    printf("%s\n\n", command_arg->usage);
  }
}

int parser (int argc, char **argv, struct broker_args *config)
{
  int c,i;
  int errfnd = 0;
  static struct option *long_options;
  long_options = (struct option *)malloc(sizeof(struct option)*COMMAND_ARGS_SIZE);
  for (i = 0; i < COMMAND_ARGS_SIZE; i++)
  {
    long_options[i].name = command_args[i].name;
    long_options[i].has_arg = command_args[i].req_args==0?no_argument:required_argument;
    long_options[i].flag = 0;
    long_options[i].val = command_args[i].key;
  }

  while (1)
  {
    int option_index = 0;

    c = getopt_long (argc, argv, "",
        long_options, &option_index);

    if (c == -1)
      break;

    for (i = 0; i < COMMAND_ARGS_SIZE; i++)
    {
      const command_args_t *command_arg = &command_args[i];
      if( c == command_arg->key)
      {
        printf("Command: %s\n", command_arg->name);
        errfnd = command_arg->function(optarg, command_arg->req_args, command_arg->usage, config);
        break;
      }
    }
    if(errfnd < 0)
    {
      printf("Error encountered\n");
      break;
    }
    if( i == COMMAND_ARGS_SIZE )
    {
      printf("Unknown command\n");
      print_help(argv[0]);
      errfnd = -1;
      break;
    }
  }
  free(long_options);
  return errfnd;
}

static int parse_version(char *subopts, uint16_t req_args, const char *usage, struct broker_args *config)
{
  int errfnd = 0;
  if(subopts != NULL)
  {
    printf("WRONG USAGE: \n %s\n", usage);
    return errfnd;
  }
  printf("%s %s %s \n", APP_VER, COMPILE_TYPE, HW_VERSION);
  printf("%s %s \n", COMPILE_MACHINE_NAME, COMPILE_TIME);
  return errfnd;
}

static int parse_help(char *subopts, uint16_t req_args, const char *usage, struct broker_args *config)
{
  int errfnd = 0;
  if(subopts != NULL)
  {
    printf("WRONG USAGE: \n %s\n", usage);
    return errfnd;
  }
  print_help("printer_app");
  errfnd = 1;
  return errfnd;
}

static int generate_conf(char *subopts, uint16_t req_args, const char *usage, struct broker_args *config)
{
  int errfnd = 0;
  if(subopts != NULL)
  {
    printf("WRONG USAGE: \n %s\n", usage);
    return errfnd;
  }
  config->generate_conf = 1;
  return errfnd;
}

static int parse_daemon(char *subopts, uint16_t req_args, const char *usage, struct broker_args *config)
{
  int errfnd = 0;
  if(subopts != NULL)
  {
    printf("WRONG USAGE: \n %s\n", usage);
    return errfnd;
  }
  config->is_daemon = 1;
  return errfnd;
}

static int set_log_level(char *subopts, uint16_t req_args, const char *usage, struct broker_args *config)
{
  char *value;
  int level = 0;
  int errfnd = 0;
  int opt = 0;
  int subarg_cnt = 0;
  enum {
    SUB_ARG_LEVEL = 0,
  };
  char *const token[] = {
    [SUB_ARG_LEVEL]   = "level",
    NULL
  };
  while (*subopts != '\0' && !errfnd)
  {
    subarg_cnt++;
    opt = getsubopt(&subopts, token, &value);
    switch (opt)
    {
      case SUB_ARG_LEVEL:
        errfnd = numeric_subopt_parse(value, token[SUB_ARG_LEVEL], &level, LOGGER_EMERG, logger_levels_nr-1);
        break;

      default:
        fprintf(stderr, "No match found "
            "for token: /%s/\n", value);
        errfnd = 1;
        break;
    }
  }
  if(errfnd || subarg_cnt != req_args)
  {
    printf("WRONG USAGE: \n %s\n", usage);
    return errfnd;
  }
  config->log_level_provided = 1;
  config->log_level = level;
  return errfnd;
}
