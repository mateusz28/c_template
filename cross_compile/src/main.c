#include <err.h>
#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct newType_c_s {
  uint8_t new;
} newType_c;

int main(int argc, char* argv[]) {
  printf("Hello new world!\n");
  return 0;
}
