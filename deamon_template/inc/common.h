/* ------------------------------ 
 * * 3CITY ELECTRONICS CONFIDENTIAL 
 * * ------------------------------ 
 * * Copyright (C) 2018 3City Electronics Sp. z o.o. - All Rights Reserved 
 * * 
 * * Unauthorized copying, usage, reproduction or distribution of this file, 
 * * via any medium is strictly prohibited unless prior written permission 
 * * is obtained from 3City Electronics Sp. z o.o. 
 * * ------------------------------ 
 * */

#ifndef COMMON_H_JXCOGK1Q
#define COMMON_H_JXCOGK1Q

#define MAX_PATH_LEN 256
#define MAX_LEVEL_STR_LEN 256
#define STARTUP_TIMEOUT_S 10

#include <stdint.h>

typedef enum {
  APP_STATUS_OK=0,
  APP_STATUS_CONF_ERR,
  APP_STATUS_ERR,
  app_status_nr
} app_status_t;


#endif /* end of include guard: COMMON_H_JXCOGK1Q */
