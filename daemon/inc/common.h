#ifndef COMMON_H_T4CPV5XY
#define COMMON_H_T4CPV5XY

#include <stdint.h>

#define ARRAY_SIZE(array) sizeof(array)/sizeof(array[0])
#define ST_CHECK(x) if(x==ERR) return ERR;

#ifndef DEBUG
#define dbPrintf(...)
#else
#define dbPrintf(...) printf("[DEB] " __VA_ARGS__)
#endif

#if DEBUG
#define syslog(priority, ...) printf(__VA_ARGS__)
#endif

typedef enum {
  OK,
  ERR
} status_t;


#endif /* end of include guard: COMMON_H_T4CPV5XY */
