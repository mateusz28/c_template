/* ------------------------------ 
 * * 3CITY ELECTRONICS CONFIDENTIAL 
 * * ------------------------------ 
 * * Copyright (C) 2018 3City Electronics Sp. z o.o. - All Rights Reserved 
 * * 
 * * Unauthorized copying, usage, reproduction or distribution of this file, 
 * * via any medium is strictly prohibited unless prior written permission 
 * * is obtained from 3City Electronics Sp. z o.o. 
 * * ------------------------------ 
 * */

#ifndef LOGGER_H_R9NMSDN8
#define LOGGER_H_R9NMSDN8

typedef enum {
	LOGGER_EMERG = 0,
	LOGGER_ALERT,
	LOGGER_CRIT,
	LOGGER_ERR,
	LOGGER_WARNING,
	LOGGER_NOTICE,
	LOGGER_INFO,
	LOGGER_DEBUG,
  LOGGER_FILE,
  logger_levels_nr
} logger_levels_t;

#define MAX_LOG_BUF 2048

#ifdef ESCCHAR
#define C_NORMAL  "\x1B[0m"  // normal
#define C_RED     "\x1B[31m" // red
#define C_GREEN   "\x1B[32m" // green
#define C_YELLOW  "\x1B[33m" // yellow
#define C_BLUE    "\x1B[34m" // blue
#define C_MAGENTA "\x1B[35m" // magenta
#define C_CYAN    "\x1B[36m" // cyan
#define C_WHITE   "\x1B[37m" // white
#define C_CLEAR   "\x1B[2J" // clear
#define C_CLR_LINE   "\x1B[2K" // clear line
#else
#define C_NORMAL  ""  // normal
#define C_RED     "" // red
#define C_GREEN   "" // green
#define C_YELLOW  "" // yellow
#define C_BLUE    "" // blue
#define C_MAGENTA "" // magenta
#define C_CYAN    "" // cyan
#define C_WHITE   "" // white
#define C_CLEAR   "" // clear
#define C_CLR_LINE   "" // clear line
#endif
#ifdef DEBUG
#define DBG(x...) logger_printf(LOGGER_DEBUG, x)
#define ERR(x...) logger_printf(LOGGER_ERR, x)
#define INF(x...) logger_printf(LOGGER_INFO, x)
//#define DBG(x...) printf(x); printf("\n")
//#define ERR(x...) printf(x); printf("\n")
//#define INF(x...) printf(x); printf("\n")
//#define DBG(x...) logger_sys(LOGGER_DEBUG, x)
//#define ERR(x...) logger_sys(LOGGER_ERR, x)
//#define INF(x...) logger_sys(LOGGER_INFO, x)
#else
#define DBG(x...) logger_sys(LOGGER_DEBUG, x)
#define ERR(x...) logger_sys(LOGGER_ERR, x)
#define INF(x...) logger_sys(LOGGER_INFO, x)
#endif


int logger_init(logger_levels_t log_level);
void logger_sys(logger_levels_t level, const char *__fmt, ...);
void logger_printf(logger_levels_t level, const char *__fmt, ...);
void logger_deinit();
void logger_commit(logger_levels_t log_level);
void logger_append(const char *__fmt, ...);
void logger_level(logger_levels_t log_level);

#endif /* end of include guard: LOGGER_H_R9NMSDN8 */
