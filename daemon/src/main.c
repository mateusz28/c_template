#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include <time.h>

#include <common.h>
#include <libDaemon.h>
#include <libInotify.h>

#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#define DAEMON_NAME "simpledaemon"
typedef struct _myNewStruct_t {
  u_int8_t buffer[20];
  u_int8_t variable;
} myNewStruct_t;

myNewStruct_t newStruct[20] = {
  {{0}, 0},
};

int main(int argc, char *argv[]) {
#if DEBUG
  syslog(LOG_INFO, "Daemon starting up in debug mode\n");
#else
  setlogmask(LOG_UPTO(LOG_DEBUG));
  openlog(DAEMON_NAME, LOG_CONS | LOG_PERROR, LOG_USER);

  syslog(LOG_INFO, "Daemon starting up\n");

  /* Deamonize */
  daemonize("/", DAEMON_NAME);
  syslog(LOG_INFO, "Daemon running\n");
#endif

  for (int i = 0; i < argc; i++) {
    syslog(LOG_INFO, "Arg %d: %s \n", i, argv[i]);
    if (i == 1)
    {
      addFileNotification(argv[1]);
    }
  }
  while (1) {
    sleep(1);
  }
}
