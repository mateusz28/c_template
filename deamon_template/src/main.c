/* ------------------------------
 * 3CITY ELECTRONICS CONFIDENTIAL
 * ------------------------------
 * Copyright (C) 2018 3City Electronics Sp. z o.o. - All Rights Reserved
 *
 * Unauthorized copying, usage, reproduction or distribution of this file,
 * via any medium is strictly prohibited unless prior written permission
 * is obtained from 3City Electronics Sp. z o.o.
 * ------------------------------
 */

#include <err.h>
#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <sys/time.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>

#include "common.h"
#include "version.h"
#include "parser.h"
#include "daemon.h"
#include "config.h"
#include "logger.h"
#include "api.h"


void print_version(void)
{
  INF("%s %s %s", APP_VER, COMPILE_TYPE, HW_VERSION);
  DBG("%s %s", COMPILE_MACHINE_NAME, COMPILE_TIME);
}

int main(int argc, char **argv)
{
  int rc = 0;
  union config_value_u status;
  union config_value_u log_level;
  struct broker_args bargs = {0};
  if(parser(argc, argv, &bargs) != 0)
  {
    return 1;
  }
  status._int = APP_STATUS_OK;
  init_config(CONFIG_PATH);
  if(bargs.generate_conf == 1)
  {
    rc = generate_default_conf();
    return rc;
  }
  if(read_config() != 0)
  {
    if(import_config_from_file(DEFAULT_CONFIG_PATH) < 0)
    {
      deinit_config();
      init_config(CONFIG_PATH);
      if(generate_default_conf() == 0)
      {
        read_config();
      }
      else
      {
        status._int = APP_STATUS_CONF_ERR;
      }
    }
  }
  if(bargs.is_daemon == 1)
  {
    daemonize("/", argv[0]);
  }
  if(bargs.log_level_provided)
  {
    logger_init(bargs.log_level);
    log_level._int = bargs.log_level;
    set_config_element(TEMPL_GLOBAL_SETTINGS, SET_GLOBAL_SETTINGS_LOG, &log_level);
  }
  else
  {
    get_config_element(TEMPL_GLOBAL_SETTINGS, SET_GLOBAL_SETTINGS_LOG, &log_level);
    logger_init(log_level._int);
  }

  DBG("Program started by User %d", getuid ());
  set_config_element(TEMPL_GLOBAL_SETTINGS, SET_GLOBAL_SETTINGS_STATUS, &status);
  config_about_runtime_constants();
  print_version();
  for (int i = 0; i < STARTUP_TIMEOUT_S; i++)
  {
    set_config_element(TEMPL_GLOBAL_SETTINGS, SET_GLOBAL_SETTINGS_STATUS, &status);
    if(status._int != APP_STATUS_OK)
    {
      ERR("Couldn't init device!\n");
      sleep(1);
    }
    else
    {
      INF("Initialized");
      break;
    }
  }
  if(status._int == APP_STATUS_OK)
  {
    for (;;)
    {
      if (rc != 0)
      {
        break;
      }
      sleep(1); // neither pause() nor sleep() portable
    }
  }
  exit(rc == 0 ? EXIT_SUCCESS : EXIT_FAILURE);
  deinit_config();
  logger_deinit();
}
