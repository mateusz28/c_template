#!/bin/bash
GREEN='\033[1;32m'
GREEN_B='\033[0;32m'
BLUE='\033[1;34m'
RED='\033[1;31m'
RED_B='\033[0;31m'
NC='\033[0m' # No Color

control_c()
{
  echo
  echo "Ctrl-c trap. Exiting with code 0"
  exit 0
}
trap control_c SIGINT

if [ "$1" == "" ]; then
  echo "No arguments provided. Exiting with code 1"
  exit 1
fi

function usage() {
echo "$(basename $0)" usage:
  echo -c Option 1
  echo -p \[\] option 2 
  echo -h help
}

while [ "$1" != "" ]; do
  case $1 in
    -p )
      shift
      if [ $1 -z ]; then
        echo "Parameter empty. Exiting with code 1"
      fi
      ;;
    -c )
      ;;
    -h )
      usage
      exit 0
      ;;
    * )
      echo "Wrong arguments provided. Exiting with code 1"
      usage
      exit 1
      ;;
  esac
  shift
done

while true; do sleep 2; done

echo -e $GREEN"SUCCESS"$NC
exit 1
