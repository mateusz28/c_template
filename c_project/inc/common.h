#ifndef COMMON_H_T4CPV5XY
#define COMMON_H_T4CPV5XY

#include <stdint.h>

#define ARRAY_SIZE(array) sizeof(array)/sizeof(array[0])
#define ST_CHECK(x) if(x==ERR) return ERR;

#ifndef DEBUG
#define dbPrintf(...)
#else
#define dbPrintf(...) printf("[DEB] " __VA_ARGS__)
#endif

#define MEASURE_FUNCTION_EXECUTE_TIME(FUNCTION) \
struct timeval  tv1, tv2; \
gettimeofday(&tv1, NULL); \
FUNCTION \
gettimeofday(&tv2, NULL); \
double timeSpent = (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 + (double) (tv2.tv_sec - tv1.tv_sec); \
DBG(C_YELLOW">>>>%d:%s T: %lf \n"C_NORMAL, __LINE__, #FUNCTION, timeSpent);


typedef enum {
  OK,
  ERR
} status_t;


#endif /* end of include guard: COMMON_H_T4CPV5XY */
