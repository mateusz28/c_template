/* ------------------------------ 
 * * 3CITY ELECTRONICS CONFIDENTIAL 
 * * ------------------------------ 
 * * Copyright (C) 2018 3City Electronics Sp. z o.o. - All Rights Reserved 
 * * 
 * * Unauthorized copying, usage, reproduction or distribution of this file, 
 * * via any medium is strictly prohibited unless prior written permission 
 * * is obtained from 3City Electronics Sp. z o.o. 
 * * ------------------------------ 
 * */

#ifndef DAEMON_H_WDHVR0MF
#define DAEMON_H_WDHVR0MF

void daemonShutdown();
void daemonize(char *rundir, char *daemonName);

#endif /* end of include guard: DAEMON_H_WDHVR0MF */
