/* ------------------------------ 
 * * 3CITY ELECTRONICS CONFIDENTIAL 
 * * ------------------------------ 
 * * Copyright (C) 2018 3City Electronics Sp. z o.o. - All Rights Reserved 
 * * 
 * * Unauthorized copying, usage, reproduction or distribution of this file, 
 * * via any medium is strictly prohibited unless prior written permission 
 * * is obtained from 3City Electronics Sp. z o.o. 
 * * ------------------------------ 
 * */

#ifndef CONFIG_H_EMV32XSI
#define CONFIG_H_EMV32XSI

#include <stdint.h>
#include "api.h"

#define CONFIG_PATH "current.cfg"
#define DEFAULT_CONFIG_PATH "default.cfg"

int generate_default_conf(void);
int save_configuration_file_path(char *path);
int import_config_from_file(char *path);
int read_config(void);
int deinit_config(void);
int init_config(char *path);
int get_config_element(list_element_t list_element, uint8_t config_element, union config_value_u *value);
int set_config_element(list_element_t list_element, uint8_t config_element, union config_value_u *value);
int set_config_about(config_user_about_t option, char *value);
int read_config_def(void);
int get_config_element_def(list_element_t list_element, uint8_t config_element, union config_value_u *value);
int get_config_element_min(list_element_t list_element, uint8_t config_element, union config_value_u *value);
int get_config_element_max(list_element_t list_element, uint8_t config_element, union config_value_u *value);
void set_config_eth_addr();
void config_about_runtime_constants();

#endif /* end of include guard: CONFIG_H_EMV32XSI */
