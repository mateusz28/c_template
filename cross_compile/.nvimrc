set makeprg=bear\ make
nnoremap <F6> :make clean<CR>
nnoremap <F7> :make<CR>
nnoremap <F9> :!./build.sh<CR>
nnoremap <F5> :!ctags -R *<CR>

nmap <F8> <Plug>(ale_fix)
nmap <C-]> <Plug>(ale_go_to_definition)
nmap <Leader>ad <Plug>(ale_go_to_definition_in_tab)
nmap <Leader>ai <Plug>(ale_hover)
nmap <Leader>ar <Plug>(ale_find_references)
let g:ale_pattern_options = {
\   '.c$': {
\       'ale_linters': ['clangtidy', 'clangd'],
\       'ale_fixers': ['clang-format'],
\   },
\   '.py$': {
\       'ale_linters': ['pylint'],
\   },
\}
let g:ale_c_parse_compile_commands = 1
let g:ale_completion_enabled = 1
let g:ale_c_clangformat_options = '-style="{BasedOnStyle: Google, IndentWidth: 2, ColumnLimit: 100, AllowShortFunctionsOnASingleLine: None, KeepEmptyLinesAtTheStartOfBlocks: false}"'
let g:ale_sign_error = '✗✗'
let g:ale_sign_warning = '∆∆'
let g:ale_statusline_format = ['⨉ %d', '⚠ %d', '']


fu! s:FindInFiles()
  echohl MatchParen
  let userInput = input(">>>Search in project:")
  echohl None
  if userInput == ''
    echohl Error
    echon 'Empty input'
    echohl None
    return
  endif
  exe "silent ALESymbolSearch " . userInput
  exe "cw"
endfu
com! FindInFiles call s:FindInFiles()
nnoremap <leader>/ :FindInFiles<CR>

