/* ------------------------------ 
   * 3CITY ELECTRONICS CONFIDENTIAL 
   * ------------------------------ 
   * Copyright (C) 2018 3City Electronics Sp. z o.o. - All Rights Reserved 
   * 
   * Unauthorized copying, usage, reproduction or distribution of this file, 
   * via any medium is strictly prohibited unless prior written permission 
   * is obtained from 3City Electronics Sp. z o.o. 
   * ------------------------------ 
   */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <syslog.h>
#include <stdint.h>
#include <stdarg.h>

#include "logger.h"
#include "common.h"

char log_buf[MAX_LOG_BUF];
uint16_t log_buf_p = 0;
int log_file = 0;
logger_levels_t current_log_level = LOGGER_DEBUG;

#define LOG_DATA_FILE_PATH "raw_data_csv.log"

void logger_deinit()
{
  closelog();
}

void logger_level(logger_levels_t log_level)
{
  setlogmask (LOG_UPTO (log_level));
}

int logger_init(logger_levels_t log_level)
{
  int rv = 0;
  if(log_level == LOGGER_FILE)
  {
    current_log_level = log_level;
    setlogmask (LOG_UPTO (LOGGER_DEBUG));
    fclose(fopen(LOG_DATA_FILE_PATH, "w"));
    log_file = 1;
  }
  else
  {
    setlogmask (LOG_UPTO (log_level));
  }
  current_log_level = log_level;
  openlog ("data_broker", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL1);
  return rv;
}

void logger_sys(logger_levels_t level, const char *__fmt, ...)
{
  va_list vlist;
  va_start(vlist, __fmt);
  vsyslog(level, __fmt, vlist);
  va_end(vlist);
}

void logger_printf(logger_levels_t level, const char *__fmt, ...)
{
  va_list vlist;
  va_start(vlist, __fmt);
  if(current_log_level >= level)
  {
    vprintf(__fmt, vlist);
    printf("\n");
  }
  va_end(vlist);
}

void logger_append_to_file(char *path)
{
  FILE *fdt = fopen(path, "a");
  log_buf[log_buf_p] = '\n';
  log_buf_p++;
  fwrite(log_buf, sizeof(uint8_t), log_buf_p, fdt);
  fclose(fdt);
}


void logger_commit(logger_levels_t log_level)
{
  switch(log_level)
  {
    case LOGGER_DEBUG:
      DBG("%s", log_buf); 
      break;
    case LOGGER_INFO:
      INF("%s", log_buf); 
      break;
    case LOGGER_ERR:
      ERR("%s", log_buf); 
      break;
    case LOGGER_FILE:
      if(log_file)
      {
        logger_append_to_file(LOG_DATA_FILE_PATH);
      }
      else
      {
        DBG("%s", log_buf); 
      }
      break;
    default:
      ERR("%s", log_buf); 
      break;
  }
  log_buf_p = 0;
}

void logger_append(const char *__fmt, ...)
{
  va_list vlist;
  va_start(vlist, __fmt);
  log_buf_p += vsnprintf(&log_buf[log_buf_p], MAX_LOG_BUF - log_buf_p,  __fmt, vlist);
  if(log_buf_p >= MAX_LOG_BUF)
  {
    logger_commit(LOGGER_ERR);
  }
  va_end(vlist);
}
