/* ------------------------------ 
 * * 3CITY ELECTRONICS CONFIDENTIAL 
 * * ------------------------------ 
 * * Copyright (C) 2018 3City Electronics Sp. z o.o. - All Rights Reserved 
 * * 
 * * Unauthorized copying, usage, reproduction or distribution of this file, 
 * * via any medium is strictly prohibited unless prior written permission 
 * * is obtained from 3City Electronics Sp. z o.o. 
 * * ------------------------------ 
 * */

#ifndef PARSER_H_IJAV6QUZ
#define PARSER_H_IJAV6QUZ

#include <stdint.h>

#define MAX_PATH_LEN 256

struct broker_args {
  uint8_t is_daemon;
  uint8_t generate_conf;
  int log_level;
  uint8_t log_level_provided;
};

int parser (int argc, char **argv, struct broker_args *config);
void print_help(char *appName);

#endif /* end of include guard: PARSER_H_IJAV6QUZ */
