#include <stdio.h> //obsluga wejsc oraz wyjsc
#include <stdlib.h> //funkcje generalnego uzytku, dynamiczna alokacja
//pamieci, arytmetyka liczb calkowitych, komunikacja z otoczeniem
#include <string.h>//przetwarzanie ciagow znakow oraz tablic
#include <unistd.h>//uslugi systemu operacyjnego oraz makra
#include <sys/types.h>//zawiera definicje typow wykorzystywanych przy 
//wywolaniach systmowych
#include <sys/socket.h> //struktury wymagane przy tworzeniu gniazd
#include <netinet/in.h> //zmienne oraz struktury do adresowania 

#define BUFFER_SIZE 256
#define PORT_NUMBER 5000
#define QUEUED_CONNECTIONS_LIMIT 5

int main(int argc, char *argv[])
{
	int sockfd, newsockfd; //deskryptory plikow, przechowują wartosci zwrocone 
				//przez wywolania systemowe call oraz accept
 
	int portno; //przechowuje numer portu, na którym serwer akceptuje połączenia

	socklen_t clilen; //przechowuje rozmiar adresu klienta, jest to wymagane 
			//przy wywolaniu systemowym accept

	char buffer[BUFFER_SIZE]; //serwer sczytuje dane z gniazda do tego bufora

	//sockaddr_in - struktura zawierająca adres, jest ona zdefiniowana w pliku nagłówkowym netinet/in.h.
	struct sockaddr_in serv_addr; //przechowuje adres serwera
	struct sockaddr_in cli_addr; //przechowuje adres klienta podlaczonego do serwera

	int n; //przechowuje wartosc zwrocona przez funkcje read i write, tj. 
		//liczbe znakow odczytanych lub zapisanych

	sockfd = socket(AF_INET, SOCK_STREAM,0); //tworzenie nowego gniazda,
	// pierwszy argument AF_UNIX - do komunikacji na tej samej maszynie lub AF_INET - pomiędzy dwoma maszynami ipv4, AF_INET6 - ip6
	// drugi argument SOCK_STREAM - rodzaj gniazda, w tym przypadku gniazdo strumieniowe
	// trzeci argument - protokol, kiedy wartosc wynosi 0 to system sam wybierze najbardziej wlasciwy protokol
	
	bzero((char *)&serv_addr, sizeof(serv_addr)); //Funkcja bzero zeruje wszystkie wartości bufora,
	//pierwszym argumentem jest wskaźnik na bufor, a drugim rozmiar bufora

	//portno = atoi("5000"); //numer portu, na ktorym serwer będzie nasluchiwal połączenia jest przekazany, jako
	//argument, a funkcja atoi() dokonuje konwersji z ciagu znaków na liczbę calkowita, zamiast tego mozna zapisac
	portno = PORT_NUMBER;

	//zmienna serv_addr jest zmienna typu struct sockaddr_in, struktura ta posiada cztery pola
	serv_addr.sin_family = AF_INET; //pierwszym jest short sin_family, zawiera kod rodziny adresowej

	serv_addr.sin_addr.s_addr = INADDR_ANY; //To pole zawiera IP hosta, stała symbolowa INADDR_ANY pozwala 
	//na automatyczne uzyskanie adresu

	serv_addr.sin_port = htons(portno); //zawiera numer portu, na ktorym bedzie odbywala sie komunikacja,
	//numer portu w network byte order zamiast standardowego host byte, do tej zamiany sluzy htons(nr portu)

	bind(sockfd, (struct sockaddr *)&serv_addr,sizeof(serv_addr));
	//wywolanie systemowe bind() laczy gniazdo z adresem hosta i numerem portu, na ktorym bedzie nasluchiwal server
	

	listen(sockfd,QUEUED_CONNECTIONS_LIMIT); //pozwala procesowi na nasluchiwanie gniazda w oczekiwaniu na potencjalne polaczenia, 
	//pierwszy argument jest deskryptorem pliku gniazda, drugi okresla liczbę polaczen, ktore zostana ustawione 
	//w kolejce do obslugi, podczas gdy proces obsluguje już jedno polaczenie

	clilen = sizeof(cli_addr); //rozmiar adresu klienta

	newsockfd = accept(sockfd, (struct sockaddr *)&cli_addr, &clilen); //accept powoduje zatrzymanie procesu, az do czasu podlaczenia
	//klienta do serwera. Proces zostaje wybudzony w momencie, gdy polaczenie zostanie nawiązane. Wywolanie accept zwraca nowy deskryptor pliku 		//i cala komunikacja powinna odbywac sie przy wykorzystaniu niniejszego deskryptora. Drugi argument jest referencja na adres klienta, a 
	//trzeci argument wskazuje rozmiar adresu

	bzero(buffer,BUFFER_SIZE); //zerowanie bufora
	n= read(newsockfd, buffer, BUFFER_SIZE-1); //odczyt danych z gniazda
	
	printf("Here is the message: %s\n", buffer); //wypisanie danych czyli zawartosci bufora

	n = write(newsockfd, "I got your message", 18); //wyslanie do klienta potwierdzenia
	
	close(newsockfd); // zamkniecie gniazda, polaczenia z klientem
	close(sockfd); // zamkniecie deskryptora gniazda serwerowego, ktory byl wykorzysany do stworzenia polaczenia z klientem 
	return 0;
}
