#!/usr/bin/env bash
. ./environment-setup-armv7at2hf-neon-fslc-linux-gnueabi
ADDR=192.168.11.245
FILE_PATH="./dziobak/ping/ping"
REMOTE_PATH=""

control_c()
{
  echo
  echo "Ctrl-c trap. Exiting with code 0"
  exit 0
}
trap control_c SIGINT

function usage() {
echo "$(basename $0)" usage:
  echo -a address suffix
  echo -r remote path
  echo -f target file
  echo -h help
}

if [[ -z ${1} ]]; then
  echo No arguments provided
  usage
  exit 1
fi

while [ "$1" != "" ]; do
  case $1 in
    -a )
      shift
      if [[ -z $1 ]]; then
        echo No address suffix provided
        exit 1
      fi
      ADDR=192.168.11.${1}
      ;;
    -r )
      shift
      if [[ -z $1 ]]; then
        echo No remote file  provided
        exit 1
      fi
      REMOTE_PATH=$1
      ;;
    -f )
      shift
      if [[ -z $1 ]]; then
        echo No input file  provided
        exit 1
      fi
      FILE_PATH=$1
      ;;
    -h )
      usage
      exit 0
      ;;
    * )
      echo "Wrong arguments provided. Exiting with code 1"
      usage
      exit 1
      ;;
  esac
  shift
done

cgdb -d ${GDB} -iex "target extended-remote ${ADDR}:4444" -iex "set confirm off" -iex "file ${FILE_PATH}" -iex "set remote exec-file ${REMOTE_PATH}" -iex "set sysroot ${SDKTARGETSYSROOT}"

